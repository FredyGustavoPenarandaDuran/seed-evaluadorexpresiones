/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import colecciones.seed.ListaCD;

/**
 *
 * @author estudiante
 */
public class Semestre {

    private byte id;
    ListaCD<Estudiante> estudiantes = new ListaCD();

    public Semestre() {
    }

    public Semestre(byte id) {
        this.id = id;
    }

    public void addEstudiante(Estudiante nuevo) {
        this.estudiantes.addFin(nuevo);
    }

    public byte getId() {
        return id;
    }

    public ListaCD<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void setId(byte id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String msg = "Semestre{" + "id=" + id;
        for (Estudiante e : this.estudiantes) {
            msg += e.toString() + "\n";
        }
        return msg + "}";
    }

}
