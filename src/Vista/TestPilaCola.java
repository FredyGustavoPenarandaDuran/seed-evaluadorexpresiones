/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import colecciones.seed.Cola;
import colecciones.seed.Pila;

/**
 *
 * @author estudiante
 */
public class TestPilaCola {

    public static void main(String[] args) {
        Pila<Integer> pila = new Pila();
        Cola<Integer> cola = new Cola();
        
        for(int i=0; i<10; i++){
            pila.push(i);
            cola.enColar(i);
        }
        
        while(!pila.esVacio()){
            System.out.print(pila.pop() + "\t");
        }
        
        System.out.println("");
        
        while(!cola.esVacio()){
            System.out.print(cola.deColar() + "\t");
        }
    }
}
