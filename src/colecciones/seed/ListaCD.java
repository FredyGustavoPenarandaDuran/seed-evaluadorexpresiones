/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones.seed;

import java.util.Iterator;

/**
 *
 * @author madarme
 */
public class ListaCD<T> implements Iterable<T>{

    private NodoD<T> cabecera;
    private int size;

    public ListaCD() {
        this.cabecera = new NodoD();
        this.cabecera.setSig(cabecera);
        this.cabecera.setAnt(cabecera);
        this.size = 0;
    }

    public void addInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabecera.getSig(), this.cabecera);
        this.cabecera.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    public void addFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabecera, this.cabecera.getAnt());
        this.cabecera.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.size++;
    }

    public void concat(ListaCD<T> l2) {

    }

    public void concat(ListaCD<T> l2, int pos) {

    }

    @Override
    public String toString() {
        String msg = "Cab <->";
        for (NodoD<T> a = this.cabecera.getSig(); a != this.cabecera; a = a.getSig()) {
            msg += a.getInfo().toString() + "<->";
        }
        return msg;
    }

    public T remove(int i) {
        try {
            NodoD<T> eliminado = this.getPos(i);
            eliminado.getSig().setAnt(eliminado.getAnt());
            eliminado.getAnt().setSig(eliminado.getSig());
            eliminado.setAnt(null);
            eliminado.setSig(null);
            this.size--;

            return eliminado.getInfo();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public boolean isVacia() {
        return this.size == 0;
    }

    private boolean isValidoPos(int i) {
        return !(this.isVacia() || i < 0 || i >= this.size);
    }

    private NodoD<T> getPos(int i) throws Exception {
        if (!this.isValidoPos(i)) {
            throw new Exception("Error de índice de la lista");
        }
        NodoD<T> aux = this.cabecera.getSig();
        while (i-- > 0) {
            aux = aux.getSig();
        }
        return aux;
    }
    
     public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    
    public void set(int i, T info) {

        try {
            this.getPos(i).setInfo(info);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        
    }

    public int getSize() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLCD(this.cabecera);
    }
}
