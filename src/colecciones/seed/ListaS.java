/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones.seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa el manejo de una lista simple enlazada genérica
 *
 * @author madarme
 */
public class ListaS<T> {

    private Nodo<T> cabeza = null;
    private int size = 0;

    public ListaS() {
    }

    public int getSize() {
        return size;
    }

    public void addInicio(T info) {
        this.cabeza = new Nodo(info, this.cabeza);
        this.size++;

    }

    public void addFin(T info) {
        if (this.isVacia()) {
            this.addInicio(info);
        } else {
            Nodo<T> nuevo = new Nodo(info, null);
            Nodo<T> ultimo;
            try {
                ultimo = getPos(this.size - 1);
                ultimo.setSig(nuevo);
                this.size++;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }

        }
    }

    private boolean isValidoPos(int i) {
        return !(this.isVacia() || i < 0 || i >= this.size);
    }

    private Nodo<T> getPos(int i) throws Exception {
        if (!this.isValidoPos(i)) {
            throw new Exception("Error de índice de la lista");
        }
        Nodo<T> aux = this.cabeza;
        while (i-- > 0) {
            aux = aux.getSig();
        }
        return aux;
    }

    @Override
    public String toString() {
        if (this.isVacia()) {
            return "No tiene elementos la lista";
        }
        String msg = "Cab->";
        for (Nodo<T> aux = this.cabeza; aux != null; aux = aux.getSig()) {
            msg += aux.getInfo().toString() + "->\t";
        }
        return msg;
    }

    public boolean isVacia() {
        return this.cabeza == null;
    }

    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    
    public void set(int i, T info) {

        try {
            this.getPos(i).setInfo(info);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        
    }

    public T remove(int i) {
        if (!this.isValidoPos(i)) {
            throw new RuntimeException("No hay datos para borrar o la posición es inválida");
        }
        Nodo<T> aux, aux2;
        aux = aux2 = this.cabeza;
        if (i == 0) {
            this.cabeza = aux2.getSig();

        } else {
            try {
                aux = this.getPos(i - 1);
                aux2 = aux.getSig();
                aux.setSig(aux2.getSig());

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
        }
        aux2.setSig(null);
        this.size--;
        return aux2.getInfo();
    }

}
